# If executing a java application, we will use the following format 
# for the Dockerfile

# #####################################################

# Uses Open JDK as base image, the tag '8-jdk-alpine' denotes the "version" we are using
FROM openjdk:8-jdk-alpine

# Set the working directory inside the container when run, here it is root
WORKDIR /

# If on Linux machine, proceed to run './gradlew build on project root before running Docker compose
# Copy the jar file that was built and generated onto current directory
COPY build/libs/<FILENAME>-0.0.1-SNAPSHOT.jar <FILENAME>.jar

EXPOSE 8080

CMD java -jar <FILENAME>.jar

# Create docker-compose.yml file with the following contents

# Note: version must be specified correctly. Field(s) may or may not exist in certain Docker-compose version
# Have ports be 8080
version: "3.9"
services:
  web:
      build: .
          ports:
	        - "8080:8080"
		  redis:
		      image: "redis:alpine"


# CD into main project directory
# Proceed to run command "docker-compose up"

MTong8@TM0766637 MINGW64 ~/Documents/repos/docker-compose (master)
$ docker-compose up
Recreating docker-compose_web_1 ... done
Starting docker-compose_redis_1 ... done
Attaching to docker-compose_redis_1, docker-compose_web_1
redis_1  | 1:C 10 Jun 2021 05:28:15.541 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
redis_1  | 1:C 10 Jun 2021 05:28:15.541 # Redis version=6.2.4, bits=64, commit=00000000, modified=0, pid=1, just started
redis_1  | 1:C 10 Jun 2021 05:28:15.541 # Warning: no config file specified, using the default config. In order to specify a config file use redis-server /path/to/redis.conf
redis_1  | 1:M 10 Jun 2021 05:28:15.541 * monotonic clock: POSIX clock_gettime
redis_1  | 1:M 10 Jun 2021 05:28:15.542 * Running mode=standalone, port=6379.
redis_1  | 1:M 10 Jun 2021 05:28:15.542 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
redis_1  | 1:M 10 Jun 2021 05:28:15.542 # Server initialized
redis_1  | 1:M 10 Jun 2021 05:28:15.542 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
redis_1  | 1:M 10 Jun 2021 05:28:15.542 * Loading RDB produced by version 6.2.4
redis_1  | 1:M 10 Jun 2021 05:28:15.542 * RDB age 40 seconds
redis_1  | 1:M 10 Jun 2021 05:28:15.542 * RDB memory usage when created 0.77 Mb
redis_1  | 1:M 10 Jun 2021 05:28:15.542 * DB loaded from disk: 0.000 seconds
redis_1  | 1:M 10 Jun 2021 05:28:15.542 * Ready to accept connections
web_1    |
web_1    |   .   ____          _            __ _ _
web_1    |  /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
web_1    | ( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
web_1    |  \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
web_1    |   '  |____| .__|_| |_|_| |_\__, | / / / /
web_1    |  =========|_|==============|___/=/_/_/_/
web_1    |  :: Spring Boot ::        (v2.3.4.RELEASE)
web_1    |
web_1    | 2021-06-10 05:28:18.145  INFO 1 --- [           main] c.t.helloworld.HelloWorldApplication     : Starting HelloWorldApplication on 80fa2056e0ec with PID 1 (/hello-world.jar started by root in /)
web_1    | 2021-06-10 05:28:18.147  INFO 1 --- [           main] c.t.helloworld.HelloWorldApplication     : No active profile set, falling back to default profiles: default
web_1    | 2021-06-10 05:28:18.949  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
web_1    | 2021-06-10 05:28:18.964  INFO 1 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
web_1    | 2021-06-10 05:28:18.964  INFO 1 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.38]
web_1    | 2021-06-10 05:28:19.029  INFO 1 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
web_1    | 2021-06-10 05:28:19.029  INFO 1 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 831 ms
web_1    | 2021-06-10 05:28:19.189  INFO 1 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
web_1    | 2021-06-10 05:28:19.330  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
web_1    | 2021-06-10 05:28:19.341  INFO 1 --- [           main] c.t.helloworld.HelloWorldApplication     : Started HelloWorldApplication in 1.512 seconds (JVM running for 1.919)

